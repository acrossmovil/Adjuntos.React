import React from 'react'

class UploadFile extends React.Component {

constructor(props){
    super(props)
    this.state = {errorMesage: "Ha ocurrido un error al adjuntar el archivo. Inténtalo nuevamente."}
    this.handleChange = this.handleChange.bind(this)
    this.handleChangeUlpoad = this.handleChangeUlpoad.bind(this)
    this.handleChangeUlpoadError = this.handleChangeUlpoadError.bind(this)
    this.SubmitFile = this.SubmitFile.bind(this)
   } 

 handleChange(event){
    this.SubmitFile(event)
     }

componentWillMount() {
 // console.log(this.props)
}

handleChangeUlpoad(event){
  document.getElementById("file").value = "";
  this.props.manejaEstadoOk(false,function(){  })
}


handleChangeUlpoadError(){
  document.getElementById("file").value = "";
  this.props.manejaEstadoError(false,function(){  })
}


SubmitFile(event){

    event.preventDefault();
    let sizeFile = event.target.files[0].size
    console.log(sizeFile)

    //Valida el tamaño de el archivo 
    if(sizeFile <= 5000000){

      fetch(this.props.contexto.url+'/Adjunto/UploadFiles/', {
      method: 'post',
      body: new FormData(document.getElementById('formupload'))
      }).then((response) => {

        //Limpia los mensajes y el input para subir archivos
        this.props.manejaEstadoOk(false,function(){  })
        this.props.manejaEstadoError(false,function(){  })
        document.getElementById("file").value = "";
        //Cambia el estado para mostrar div ok o de error
        if(response.status === 200){
          this.props.manejaEstadoOk(true)
        }else{
          this.props.manejaEstadoError(true)
        }
        console.log(this.props.errorMesage)
         console.log(response.status)
        return response.json()     
      })
      .then((data) => {
        if(this.props.errorState === true){
          this.setState({errorMesage: data},function(){
        })
      }
      })
      .catch((error) => {
        this.setState({errorMesage: 'Ha ocurrido un error al adjuntar el archivo. Inténtalo nuevamente.'},function(){
          this.props.manejaEstadoError(true)
        })
      })
    }
    else{
    
       this.setState({errorState: true}, function() {
          if(this.state.errorState === true){
             //Limpia los mensajes y el input para subir archivos
            this.props.manejaEstadoOk(false,function(){  })
            this.props.manejaEstadoError(false,function(){  })
            document.getElementById("file").value = "";  
            this.setState({errorMesage: 'El tamaño del archivo excede 5 MB.'},function(){
              this.props.manejaEstadoError(true)
            })
          }
        })
    }
  }



  
  render() {
console.log(this.state.errorMesage)
 

    if(this.props.uploadState){
    return (
      
<div className="panel panel-primary upload-box"> 
  <div className="panel-body">
  <div className="container-fluid">
     <form id="formupload">
        <input id="file" name="file" type="file" className="file" onChange={this.handleChange}/>
        <input type="hidden" name="entidad" value={this.props.entidad}/>
        <input type="hidden" name="funcionalidad" value={this.props.funcionalidad}/>
        <input type="hidden" name="token" value={this.props.token}/> 
        <input type="hidden" name="contrato" value={this.props.contrato}/> 
      </form>
      <br/>
        </div> 
               
      </div>
    <div className="alert alert-success" display="none" role="alert"> <button type="button" className="close" data-dismiss="alert" aria-label="Cerrar" onClick={this.handleChangeUlpoad}><span aria-hidden="true">×</span></button>
  El archivo ha sido adjuntado correctamente.</div>
    </div>
    )
    }
    else if(this.props.errorState){
      return (
      
<div className="panel panel-primary upload-box"> 
  <div className="panel-body">
  <div className="container-fluid">
     <form id="formupload">
        <input id="file" name="file" type="file" className="file" onChange={this.handleChange}/>
        <input type="hidden" name="entidad" value={this.props.entidad}/>
        <input type="hidden" name="funcionalidad" value={this.props.funcionalidad}/>
        <input type="hidden" name="token" value={this.props.token}/> 
        <input type="hidden" name="contrato" value={this.props.contrato}/>   
      </form>
      <br/>
        </div> 
               
      </div>
    <div className="alert alert-danger" display="none" role="alert"> <button type="button" className="close" data-dismiss="alert" aria-label="Cerrar" onClick={this.handleChangeUlpoadError}><span aria-hidden="true">×</span></button>
{this.state.errorMesage}</div>
    </div>
    )
    }else{
       return (
      
<div className="panel panel-primary upload-box">
  <div className="panel-body">
  <div className="container-fluid">
     <form id="formupload">
        <input id="file" name="file" type="file" className="file" onChange={this.handleChange}/>
        <input type="hidden" name="entidad" value={this.props.entidad}/>
        <input type="hidden" name="funcionalidad" value={this.props.funcionalidad}/>
        <input type="hidden" name="token" value={this.props.token}/> 
        <input type="hidden" name="contrato" value={this.props.contrato}/>  
      </form>
      <br/>
        </div> 
               
      </div>
    </div>
    )
    }
  }
}

export default UploadFile



