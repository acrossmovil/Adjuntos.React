import React, { Component } from 'react'
import Modal from 'react-modal'
import AdjuntoApp from './AdjuntoApp'
import UploadFile from './UploadFile'

  const customStylesModal = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    transform             : 'translate(-50%, -50%)',
    width                 : '500px',
    border                : '0',
    boxShadow             : '0 1px 5px rgba(0,0,0,0.25)',
    transition            : 'width ease 2s',
    padding               : '16px'
  }
}

class PopUp extends Component{
  
  constructor(props){
    super(props);

    //Variables de Estado
    this.state = {modalIsOpen: false}
    this.state = {uploadState: false}
    this.state = {errorState: false}
    this.state = {adjuntos: [] }
    this.state = {estadoContrato: 1}

    //Funciones Modal
    this.openModal  = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.afterOpenModal = this.afterOpenModal.bind(this)

    //Funciones adjuntos
    this.componentWillMount = this.componentWillMount.bind(this)
    this.handleChangeData   = this.handleChangeData.bind(this)
    this.adjuntosGet        = this.adjuntosGet.bind(this) //Carga el listado de adjuntos

    //Funciones para manejo de mensajes de error
    this.manejaEstadoError = this.manejaEstadoError.bind(this)
    this.manejaEstadoOk     = this.manejaEstadoOk.bind(this)  
  } 


    componentWillMount() { 
      this.adjuntosGet()
    }

    adjuntosGet(){
      var misCabeceras = new Headers({
        'Access-Control-Allow-Origin':'*',
        'CAccess-Control-Allow-Headers':'Content-Type,x-requested-with,Authorization,Access-Control-Allow-Origin',
        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
      });
      var miInit = { method: 'GET',headers: misCabeceras,mode: 'cors'}

      let urlapi = this.props.contexto.url+'/Adjunto/Get?id=' + document.getElementById('adjuntos').getAttribute('data-entidad') +'&token=' + (encodeURIComponent(document.getElementById('adjuntos').getAttribute('data-token'))+'&funcionalidad=' + document.getElementById('adjuntos').getAttribute('data-funcionalidad'))
      if(document.getElementById('adjuntos').getAttribute('data-entidad') !== undefined && document.getElementById('adjuntos').getAttribute('data-entidad') !==null &&document.getElementById('adjuntos').getAttribute('data-entidad') !=="null") {
      
      fetch(urlapi,miInit)
      .then((response) => {
        return response.json()
      })
      .then((data) => {
        this.setState({ adjuntos: data },function(){
          this.setState({estadoContrato: 1}, function() { })
        })
      }).catch((err) =>{
        this.setState({estadoContrato: 0}, function() { })
      })
      }
    }

    manejaEstadoError(estado){
      this.setState({errorState: estado},function(){
          this.componentWillMount()
      }.bind(this))
    }

    manejaEstadoOk(estado){
      this.setState({uploadState: estado},function(){
          this.componentWillMount()
      }.bind(this))
    }

    handleChangeData(){
      this.componentWillMount()
    }

    openModal(){
      this.setState({modalIsOpen: true},function(){
        this.componentWillMount()
      })
    } 

    afterOpenModal(){
      //
    } 

    closeModal(){
      this.setState({modalIsOpen: false},function(){
        this.manejaEstadoError(false)
        this.manejaEstadoOk(false)
      })
    } 


  render() {

    if(this.state.estadoContrato === 1){
    return (
      <div>
        <button type="button" id="btnAdjunto" className="boton boton-azul" data-toggle="modal" onClick={this.openModal}>Ver adjuntos</button>

        <Modal contentLabel="Mod" isOpen={this.state.modalIsOpen} onAfterOpen={this.afterOpenModal} onRequestClose={this.closeModal}  style={customStylesModal} >
          <div className="modal-header">
           
          
              <div className="boton-cerrar close"  onClick={this.closeModal}></div>
            
            <h4 className="modal-title" id="myModalLabel">Archivos adjuntos  {document.getElementById('adjuntos').getAttribute('data-titulo')} </h4>
          </div>

                <div className="modal-body">



                 <AdjuntoApp contexto={this.props.contexto} 
                    entidad={document.getElementById('adjuntos').getAttribute('data-entidad')}
                    handleChangeData={this.handleChangeData}
                    adjuntos={this.state.adjuntos}
                    token={document.getElementById('adjuntos').getAttribute('data-token')}
                    uploadState={this.state.uploadState}
                    errorState={this.state.errorState}
                    manejaEstadoError={this.manejaEstadoError}
                    manejaEstadoOk={this.manejaEstadoOk}
                    componentWillMount={this.componentWillMount}
                    />

				          <UploadFile contexto={this.props.contexto}
                    entidad={document.getElementById('adjuntos').getAttribute('data-entidad')}
                    funcionalidad={document.getElementById('adjuntos').getAttribute('data-funcionalidad')}
                    token={document.getElementById('adjuntos').getAttribute('data-token')}
                    contrato={document.getElementById('adjuntos').getAttribute('data-contrato')}
                    adjuntos={this.state.adjuntos} 
                    handleChangeData={this.handleChangeData}
                    uploadState={this.state.uploadState}
                    errorState={this.state.errorState}
                    manejaEstadoError={this.manejaEstadoError}
                    manejaEstadoOk={this.manejaEstadoOk}
                    componentWillMount={this.componentWillMount}
                    />
                </div>
                <div className="modal-footer">
                  <button type="button" className="boton boton-azul" data-dismiss="modal" onClick={this.closeModal}>Cerrar</button>
                </div>   
        </Modal>

      </div>
    )
    }
    else{
            return (
      <div>
        <button type="button" id="btnAdjunto" className="boton boton-azul" data-toggle="modal" onClick={this.openModal}>Ver adjuntos</button>
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStylesModal} >
 
          
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-label="Cerrar" onClick={this.closeModal}>
                    <div className="boton-cerrar"></div>
                  </button>
                  <h4 className="modal-title" id="myModalLabel">Archivos adjuntos  {document.getElementById('adjuntos').getAttribute('data-titulo')} </h4>
                </div>
                <div className="modal-body">
			    <div className="alert alert-danger" display="none" role="alert">En estos momentos no es posible mostrar la lista de archivos adjuntos.</div>
                </div>
                <div className="modal-footer">
                  <button type="button" className="boton boton-azul" data-dismiss="modal" onClick={this.closeModal}>Cerrar</button>
                </div>   
        </Modal>
      </div>
    )

    }
  
  }
};

export default PopUp;