import React from 'react'


class AdjuntoRow extends React.Component {
      constructor(props){
      super(props)
      this.openFile = this.openFile.bind(this)
      this.deleteFile =  this.deleteFile.bind(this)
    } 

    openFile(){
      this.props.manejaEstadoError(false)
      this.props.manejaEstadoOk(false)
      window.location.href =this.props.contexto.url+'/Adjunto/GetFile?id=' + this.props.IdArchivo + '&token=' + encodeURIComponent(document.getElementById('adjuntos').getAttribute('data-token')); 
      return false;     
    } 

    deleteFile(event){
      fetch(this.props.contexto.url+'/Adjunto/DelFile2?id=' + this.props.IdArchivo + '&token='+ encodeURIComponent(document.getElementById('adjuntos').getAttribute('data-token')),{
      method: 'POST',
      })
      .then((response) => {
        this.props.manejaEstadoError(false)
        this.props.manejaEstadoOk(false)
      })
      .catch((err) =>{
        this.props.manejaEstadoError(true)
      })
    }

  render() {

    return(
      <div className="list-group">
        <div className="list-group-item" > 
          <button type="button" className="close" id="eliminar" data-balloon="Eliminar" data-balloon-pos="up" onClick={this.deleteFile}><span className="nuboxFont nubox-borrar" aria-hidden="true"></span></button>
          <button type="button"  className="close" id="descargar" data-balloon="Descargar" data-balloon-pos="up" onClick={this.openFile}><span className="nuboxFont nubox-descargar" aria-hidden="true"></span></button>
        {this.props.NombreArchivo} 
        </div> 
      </div>
     )
  }
}

export default AdjuntoRow