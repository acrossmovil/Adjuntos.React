import React from 'react'
import AdjuntoRow from './AdjuntoRow'

class AdjuntoList extends React.Component {

  render() {
    return (
      <div className="panel panel-primary adjuntos">
          <div className="panel-body">
              <div className="container-fluid">
                    <ul className="list-group App-scroll">
                      {
                        this.props.listado.map((adjunto) => {
                          return <AdjuntoRow key={ adjunto.IdArchivo }
                                            NombreArchivo={adjunto.NombreArchivo}
                                            IdArchivo={adjunto.IdArchivo}
                                            contexto={this.props.contexto}
                                            handleChangeData={this.props.handleChangeData}
                                            token={this.props.token}
                                            manejaEstadoError={this.props.manejaEstadoError}
                                            manejaEstadoOk={this.props.manejaEstadoOk}
                                            errorState={this.props.errorState}
                                            uploadState={this.props.uploadState}
                                              />
                        })
                      }
                    </ul>
                </div>
              </div>
          </div>
    )
  }
}

export default AdjuntoList