import React from 'react'
import AdjuntoList from './AdjuntoList'

class AdjuntoApp extends React.Component {



  componentWillMount(){
    this.props.handleChangeData() 
  }

  render() {
       if (this.props.adjuntos.length > 0) {
      return ( 
           <AdjuntoList listado={this.props.adjuntos} 
           contexto={this.props.contexto} handleChangeData={this.props.handleChangeData}
           token={this.props.token}
           manejaEstadoError={this.props.manejaEstadoError}
           manejaEstadoOk={this.props.manejaEstadoOk}
           errorState={this.props.errorState}
           uploadState={this.props.uploadState}
           />
       
      )
    } else if((this.props.adjuntos.length === 0)){
      return <p className="text-center sin-adjuntos">No hay archivos adjuntos</p>
    }else{
      return <p className="text-center">Cargando adjuntos...</p>
    }
  }


}

export default AdjuntoApp