import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import './bootstrap.min.css';
import '../css/estilos.css';
import '../css/balloon.min.css';
import '../fonts/nubox-font.css';
 
setTimeout(function() {
  renderStartDiv() 
}, 100);

function renderStartDiv()
{
  if (document.getElementById('adjuntos') !==undefined && document.getElementById('adjuntos') !==  null ) {
      let root = document.getElementById('adjuntos')
      let entidad = root.getAttribute('data-entidad')
      let funcionalidad = root.getAttribute('data-funcionalidad')
      let token = root.getAttribute('data-token')
      let contrato = root.getAttribute('data-contrato')
      let titulo = root.getAttribute('data-titulo')
    
      ReactDOM.render(
        <App entidad={entidad}
        funcionalidad={funcionalidad}
        token={token}
        contrato={contrato}
        titulo={titulo}/>,
        root
    ); 
  } else {
    setTimeout(function() {
      renderStartDiv() }, 100);
  }
}
 