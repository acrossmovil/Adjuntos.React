import React, { Component } from 'react';
import PopUp from './component/PopUp'

const contexto = {
  url:   'http://10.233.32.216/Adjuntos.API'
}

class App extends Component {
  
  render() {
  return (
    <div className="App">
      <PopUp 
        entidad={this.props.entidad}
        funcionalidad={this.props.funcionalidad}
        token={this.props.token}
        contrato={this.props.contrato}
        titulo={this.props.titulo}
        contexto={contexto}
      />
    </div>
  )
  }
  }
export default App;